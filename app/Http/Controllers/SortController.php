<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Sorting;

class SortController extends Controller
{
  public function __construct() {
    $this->sort = new Sorting();
  }
  public function urutkanAngkaASC(){
    // Array angka yang akan diurutkan
    $angka = [77, 11, 66, 75, 77, 1, 9, 3, 7, 58, 7, 69, 36, 9, 33, 33, 33, 31, 21, 98, 1];

    // Panggil fungsi untuk mengurutkan array
    $angkaTerurut = $this->sort->bubbleSortASC($angka);

    // Panggil tampilan dan kirimkan data angka yang sudah diurutkan
    return view('urutkan', ['angkaTerurut' => $angkaTerurut]);
  }

  public function urutkanAngkaDESC(){
    // Array angka yang akan diurutkan
    $angka = [77, 11, 66, 75, 77, 1, 9, 3, 7, 58, 7, 69, 36, 9, 33, 33, 33, 31, 21, 98, 1];

    // Panggil fungsi untuk mengurutkan array
    $angkaTerurut = $this->sort->bubbleSortDESC($angka);

    // Panggil tampilan dan kirimkan data angka yang sudah diurutkan
    return view('urutkan', ['angkaTerurut' => $angkaTerurut]);
  }
  
}
