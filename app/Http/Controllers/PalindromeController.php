<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Palindrome;

class PalindromeController extends Controller
{
  public function cekPalindrom($kata){
    $palindrom = new Palindrome();
    $hasilPemeriksaan = $palindrom->isPalindrom($kata)? 'Kata Palindrom' : 'Bukan Palindrom';

    // Menampilkan view dengan hasil pemeriksaan
    return view('palindrom', ['hasilPemeriksaan' => $hasilPemeriksaan, 'kata' => $kata]);
  }

  
}
