<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\FIFOQueue;

class QueueController extends Controller
{
  public function antrian(){
    $antrian = new FIFOQueue();
    $output=[];

    $dataInput = [77, 11, 66, 75, 77, 1, 9, 3, 7, 58, 7, 69, 36, 9, 33, 33, 33, 31, 21, 98, 1];
    
    foreach ($dataInput as $data) {
      $output[] = $antrian->enqueue($data);
      $output[] = $antrian->display();
    }

    // Mengambil data dari antrian
    $output[] = $antrian->dequeue();
    $output[] = $antrian->display();

    return view('antrian', ['output' => $output]);
  }

}
