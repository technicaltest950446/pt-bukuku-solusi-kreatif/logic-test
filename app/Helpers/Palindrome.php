<?php
namespace App\Helpers;

class Palindrome{
  function isPalindrom($kata){
    // Menghapus spasi dan mengubah menjadi huruf kecil
    $kata = strtolower(str_replace(' ', '', $kata));

    // // Membalikkan kata
    // $kataTerbalik = strrev($kata);

    // // Memeriksa apakah kata asli sama dengan kata terbalik
    // if ($kata === $kataTerbalik) {
    //   $hasilPemeriksaan = "Kata Palindrom";
    // } else {
    //   $hasilPemeriksaan = "Bukan Palindrom";
    // }

    $panjang = strlen($kata);

    // Memeriksa apakah kata merupakan palindrom
    for ($i = 0; $i < $panjang / 2; $i++) {
      if ($kata[$i] != $kata[$panjang - $i - 1]) {
        return false;
      }
        
    }
    return true;
  }
}