<?php
namespace App\Helpers;

class FIFOQueue{
    private $antrian = [];
    private $jumlahMax = 5;

    public function enqueue($data)
    {
        // Cek apakah antrian sudah penuh
        if (count($this->antrian) >= $this->jumlahMax) {
            return "Antrian sudah penuh. Data '$data' tidak dapat dimasukkan.\n";
        }

        // Tambahkan data ke antrian
        $this->antrian[] = $data;
        return "Data '$data' berhasil dimasukkan ke dalam antrian.\n";
    }

    public function dequeue()
    {
        // Cek apakah antrian kosong
        if (empty($this->antrian)) {
            return "Antrian kosong. Tidak ada data yang dapat diambil.\n";
        }

        // Ambil data dari depan antrian (FIFO)
        $data = array_shift($this->antrian);
        return "Data '$data' berhasil diambil dari antrian.\n";
    }

    public function display()
    {
        // Tampilkan isi antrian
        if (empty($this->antrian)) {
            return "Antrian kosong.\n";
        } else {
            return "Isi antrian: " . implode(', ', $this->antrian) . "\n";
        }
    }
}
