<?php
namespace App\Helpers;

class Sorting{
  function bubbleSortASC(array $angka){
    $n = count($angka);

    // Algoritma Bubble Sort
    for ($i = 0; $i < $n - 1; $i++) {
      for ($j = 0; $j < $n - $i - 1; $j++) {
        if ($angka[$j] > $angka[$j + 1]) {
          $temp = $angka[$j];
          $angka[$j] = $angka[$j + 1];
          $angka[$j + 1] = $temp;
        }
      }
    }

    return $angka;
  }

  function bubbleSortDESC(array $angka){
    $n = count($angka);

    // Algoritma Bubble Sort
    for ($i = 0; $i < $n - 1; $i++) {
      for ($j = 0; $j < $n - $i - 1; $j++) {
        if ($angka[$j] < $angka[$j + 1]) {
          $temp = $angka[$j];
          $angka[$j] = $angka[$j + 1];
          $angka[$j + 1] = $temp;
        }
      }
    }

    return $angka;
  }
}