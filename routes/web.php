<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SortController;
use App\Http\Controllers\QueueController;
use App\Http\Controllers\PalindromeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/soalsatu', [SortController::class, 'urutkanAngkaASC'])->name('soal.satu');
Route::get('/soaldua', [SortController::class, 'urutkanAngkaDESC'])->name('soal.dua');
Route::match(['get', 'post'], '/soaltiga/{kata?}', [PalindromeController::class, 'cekPalindrom'])->name('soal.tiga');
Route::get('/soalempat', [QueueController::class, 'antrian'])->name('soal.empat');
