<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Antrian Output</title>
</head>
<body>
    <h1>Antrian Output</h1>
    
    @foreach ($output as $line)
        <p>{{ $line }}</p>
    @endforeach
</body>
</html>
