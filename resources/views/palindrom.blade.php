<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Test Logic</title>
</head>
<body>
  <form method="GET" action="{{ route('soal.tiga', ['kata' => $kata])}}" id="palindromForm">
    @csrf
    <label for="kata">Masukkan Kata:</label>
    <input type="text" name="kata" id="kata" value="{{ $kata }}" required>
    <button type="submit" onclick="submitForm()">Cek Palindrom</button>
  </form>
  
  <h1>Hasil Pemeriksaan Palindrom</h1>
  <p>{{ $hasilPemeriksaan }}</p>
  
  <script>
    function submitForm() {
        var kata = document.getElementById('kata').value;
        var form = document.getElementById('palindromForm');
        form.action = "{{ route('soal.tiga') }}/" + kata;
        form.submit();
    }
</script>
</body>
</html>
